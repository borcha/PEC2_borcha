import { Component, Input } from '@angular/core';
import { Contact } from '../../shared/models/contact'
import { ContactListService } from '../contact-list.service';
import { Router } from '@angular/router';

@Component({
  selector: 'contact-item',
  templateUrl: './contact-item.component.html',
  styleUrls: ['./contact-item.component.scss']
})
export class ContactItemComponent {

  @Input() id: number;
  @Input() firstNameTxt: string;
  @Input() lastNameTxt: string;
  @Input() telephoneTxt: string;

  constructor(
    private router: Router,
    private contactListService: ContactListService
   ) {}

  goDetail() {
  	this.router.navigate(['edit', this.id]);
  }

  removeContact() {
  	if (confirm('¿Eliminar el contacto?')) {
      this.contactListService.removeContact(this.id);
    }
  }
}
