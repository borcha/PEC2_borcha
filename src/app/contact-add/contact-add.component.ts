import { Component } from '@angular/core';
import { Contact } from '../../shared/models/contact';
import { ContactListService } from '../contact-list.service';

@Component({
  selector: 'contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.scss']
})
export class ContactAddComponent {

  firstNameTxt: string;
  lastNameTxt: string;
  telephoneTxt: string;

  constructor(
    private contactListService: ContactListService
  ) {
    this.resetContact();
  }

  saveNewContact() {
    if ([this.firstNameTxt, this.lastNameTxt, this.telephoneTxt].some(t => t.trim().length == 0)) {
      alert('Todos los campos son obligatorios');
      return;
    }
    this.contactListService.addContact(this.firstNameTxt, this.firstNameTxt, this.telephoneTxt);
    this.resetContact();
  }

  resetContact() {
    this.firstNameTxt = '';
    this.lastNameTxt = '';
    this.telephoneTxt = '';   
  }

  noData() {
    // Se podría haber hecho usando FormGroup y usando las propiedades pristine
    return ![this.firstNameTxt, this.lastNameTxt, this.telephoneTxt].some(t => t.length > 0);
  }
}
