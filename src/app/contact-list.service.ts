import { Injectable } from '@angular/core';
import { Contact } from '../shared/models/contact'

@Injectable({
  providedIn: 'root'
})
export class ContactListService {
  private contactsSeq: number;
  private contacts: Contact[];

  constructor() {
    this.restoreData();
  }

  getContacts () {
    return this.contacts;
  }

  removeContact(contactId: number) {
    // Filtramos la lista y nos quedamos con todos los contactos excepto
    // el que tenga el id que queremos eliminar
    this.contacts = this.contacts.filter((contact) => contact.id != contactId);
    this.persistData();
  }

  addContact(firstName: string, lastName: string, tel: string) {
    // Incrementamos la secuencia con el número de contactos creados
    this.contacts.push({id: this.contactsSeq++, firstName, lastName, tel});
    this.persistData();
  }

  getContact(id: number) {
    // Buscamo el contacto con el id indicado
    return this.contacts.find(contact => contact.id == id);
  }

  updateContact(contactData: Contact) {
    let contact = this.getContact(contactData.id);

    // Asignamos todas las popiedades del contacto pasado por parámetro
    Object.assign(contact, contactData);
    this.persistData();
  }

  persistData() {
    localStorage.setItem('contactsSeq', JSON.stringify(this.contactsSeq));
    localStorage.setItem('contacts', JSON.stringify(this.contacts));
  }

  restoreData() {
    this.contactsSeq = this.restoreOrDefault('contactsSeq', 1);
    this.contacts = this.restoreOrDefault('contacts', [{
      id: 0, firstName: 'John', lastName: 'Doe', tel: '999-999-999'
    }]);
  }

  restoreOrDefault(item:string, value:any):any {
    let data = localStorage.getItem(item);
    if (data != undefined) {
      value = JSON.parse(data);
    }
    return value;
  }
}
