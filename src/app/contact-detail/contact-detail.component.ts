import { Component, OnInit } from '@angular/core';
import { Contact } from '../../shared/models/contact';
import { ContactListService } from '../contact-list.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent implements OnInit {

  contact: Contact;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private contactListService: ContactListService
  ) {}

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    // Creamos una copia del objeto para poder sus campos
    this.contact = Object.assign({}, this.contactListService.getContact(id));
  }

  save() {
    if ([this.contact.firstName, this.contact.lastName, this.contact.tel].some(t => t.trim().length == 0)) {
      alert('Todos los campos son obligatorios');
      return;
    }
    this.contactListService.updateContact(this.contact);
    this.back();
  }

  cancel() {
    this.back(); 
  }

  remove() {
    if (confirm('¿Eliminar el contacto?')) {
      this.contactListService.removeContact(this.contact.id);
      this.back();
    }
  }

  back() {
    this.location.back();
  }
}
 