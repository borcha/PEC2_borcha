import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'contact-app',
  templateUrl: './contact-app.component.html',
  styleUrls: ['./contact-app.component.scss']
})
export class ContactAppComponent {

  @Input() showBack: boolean = false;
  title: string;
  author: string;

  constructor(
    private location: Location
   ) { 
    this.title = "Contacts Agenda"
    this.author = "borcha"
  }

  goBack(): void {
    this.location.back();
  }
}
