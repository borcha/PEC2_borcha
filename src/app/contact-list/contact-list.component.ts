import { Component } from '@angular/core';
import { Contact } from '../../shared/models/contact';
import { ContactListService } from '../contact-list.service';

@Component({
  selector: 'contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent {

  constructor(private contactListService: ContactListService) {}

  getContacts() {
    return this.contactListService.getContacts();
  }
}
